function generateRandomAlphabet() {
    const alphabet = 'abcdefghijklmnopqrstuvwxyz';
    const index = Math.floor(Math.random() * alphabet.length);
    return alphabet[index];
  }

module.exports = {
    generateRandomAlphabet
}
